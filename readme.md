

[link to the project](https://irena-peshova-apollo.netlify.app)

dependencies: 
* @apollo/client: "^3.4.16"
* @testing-library/jest-dom: "^5.14.1"
* @testing-library/react: "^11.2.7"
* @testing-library/user-event: "^12.8.3"
* graphql: "^15.6.1"
* hookrouter: "^1.2.5"
* react: "^17.0.2"
* react-dom: "^17.0.2
* react-scripts: "4.0.3"
* web-vitals: "^1.1.2"
