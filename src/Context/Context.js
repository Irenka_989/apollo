import { createContext } from "react";
import { useQuery, gql } from "@apollo/client";
const FILMS_QUERY = gql`
  query {
    launchesPast(limit: 10) {
      id
      mission_name
      launch_date_local
      launch_site {
        site_name_long
      }
      links {
        article_link
        flickr_images
      }
      ships {
        name
        home_port
        image
        type
      }
      rocket {
        rocket_name
        rocket_type
        rocket {
          company
          country
          description
          first_flight
          first_stage {
            burn_time_sec
            engines
            fuel_amount_tons
          }
          second_stage {
            burn_time_sec
            engines
            fuel_amount_tons
          }
          stages
        }
      }
    }
  }
`;

export const ApolloContext = createContext();

export const ApolloProvider = ({ children }) => {
  const { data, loading, error } = useQuery(FILMS_QUERY);
  
  const obj = {
  data,
  loading,
  error
  };

  return (
    <ApolloContext.Provider value={obj}>{children}</ApolloContext.Provider>
  );
};
