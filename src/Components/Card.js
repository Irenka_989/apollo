import React from "react";
import { navigate } from "hookrouter";

const Card = ({ launch }) => {
  const redirect = () => {
    navigate(`/details/${launch.id}`);

  };
  const date = launch.launch_date_local.substring(0, 10);

  return (
    <div onClick={redirect} className="card">
      <h3>{launch.mission_name}</h3>
      <p className="site">{launch.launch_site.site_name_long}</p>
      <p>
        launch date: <span>{date}</span>
      </p>
      <div className="card-img">
        <img
          src={
            launch.links.flickr_images[0]
              ? launch.links.flickr_images[0]
              : launch.ships[0].image
          }
          alt="rocket"
        />
      </div>
    </div>
  );
};

export default Card;
