import React, { useContext } from "react";
import { ApolloContext } from "../Context/Context";

const RocketDetails = ({ id }) => {
  const { data } = useContext(ApolloContext)
  const rocket = data.launchesPast.find((el) => el.id === id);
  
 
  return (
    <div className="RocketDetails">
      <div className="opacity"></div>
      <h1>{rocket.mission_name}</h1>
      <div className="content">
        <div className="rocket-div">
          <h2>
            Rocket name: <span>{rocket.rocket.rocket_name}</span>
          </h2>
          <h3>Rocket type: {rocket.rocket.rocket_type}</h3>
          <h3>Company: {rocket.rocket.rocket.company}</h3>
          <h3>Country: {rocket.rocket.rocket.country}</h3>
          <h3>Description: {rocket.rocket.rocket.description}</h3>
          <h3>First flight: {rocket.rocket.rocket.first_flight}</h3>
          <h3>Stages: {rocket.rocket.rocket.stages}</h3>
          <div className="stages"> <ul>
            First Stage
            <li>
              burn time in sec: {rocket.rocket.rocket.first_stage.burn_time_sec}
            </li>
            <li>engines: {rocket.rocket.rocket.first_stage.engines}</li>
            <li>
              fuel amount in tons: {rocket.rocket.rocket.first_stage.fuel_amount_tons}
            </li>
          </ul>
          <ul>
            Second Stage
            <li>
              burn time in sec: {rocket.rocket.rocket.second_stage.burn_time_sec}
            </li>
            <li>engines: {rocket.rocket.rocket.second_stage.engines}</li>
            <li>
              fuel amount in tons: {rocket.rocket.rocket.second_stage.fuel_amount_tons}
            </li>
          </ul>
          </div>
        </div>
        <div className="ships">
          {rocket.ships.length > 0 &&
            rocket.ships.map((el) => (
              <div key={el.id} className="ships-inner">
                <p>{el.name}</p>
                <img src={el.image} alt="ship" />
                <p>{el.home_port}</p>
                <p>Type: {el.type}</p>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};

export default RocketDetails;
