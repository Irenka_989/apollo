import React, { useContext } from "react";
import Card from "../Components/Card";
import { ApolloContext } from "../Context/Context";

export default function Home() {
  const { data, loading, error } = useContext(ApolloContext);

  if (loading) return "Loading...";
  if (error) return <pre>{error.message}</pre>;

  return (
    <div className="main">
      <h1>SpaceX Launches</h1>
      <div className="cardContainer">
        {data.launchesPast.map((launch) => (
          <Card key={launch.id} launch={launch}/>
        ))}
      </div>
    </div>
  );
}
