import Home from "../Pages/Home";
import RocketDetails from "../Pages/RocketDetails";


const routes = {
  "/": () => <Home />,
  "/details/:id": ({id}) => <RocketDetails id={id} />,
};

export default routes;
