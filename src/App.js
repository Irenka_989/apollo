import './App.css';
import routes from "./Router/Routes"
import {useRoutes} from "hookrouter"
import {ApolloProvider} from "../src/Context/Context"




function App() {

  const routesResults = useRoutes(routes)

  return (
    <div>
     <ApolloProvider>
     {routesResults || <div>ERROR 404</div>}
     </ApolloProvider>
    </div>
  );
}


export default App;
